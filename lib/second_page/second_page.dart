import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  static const String id = 'SecondScreen';

  @override
  Widget build(BuildContext context) {
    final String image = ModalRoute.of(context).settings.arguments;

    return Container(
      color: ThemeData.dark().canvasColor,
      child: image != null
          ? CachedNetworkImage(
              imageUrl: image,
              progressIndicatorBuilder: (context, url, downloadProgress) {
                return Center(
                  child: CircularProgressIndicator(
                      value: downloadProgress.progress),
                );
              },
              errorWidget: (context, url, error) => Icon(Icons.error),
            )
          : Container(),
    );
  }
}
