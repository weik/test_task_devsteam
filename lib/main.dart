import 'package:flutter/material.dart';

import 'get_started_page/get_started_page.dart';
import 'home_page/home_page.dart';
import 'second_page/second_page.dart';

void main() => runApp(MyTestApp());

class MyTestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
        accentColor: Colors.deepOrange,
      ),
      initialRoute: GetStartedPage.id,
      routes: {
        GetStartedPage.id: (context) => GetStartedPage(),
        HomePage.id: (context) => HomePage(),
        SecondScreen.id: (context) => SecondScreen(),
      },
    );
  }
}
