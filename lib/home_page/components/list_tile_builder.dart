import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../second_page/second_page.dart';
import 'card_tile.dart';

class ListTileBuilder extends StatefulWidget {
  @override
  _ListTileBuilderState createState() => _ListTileBuilderState();
}

class _ListTileBuilderState extends State<ListTileBuilder> {
  List data;
  String api =
      'http://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0';

  Future<String> getJSONData() async {
    var response = await http.get(
      api,
    );
    if (response.statusCode == 200) {
      setState(() {
        data = json.decode(response.body);
        print(data);
      });
    }
    return 'Successful';
  }

  @override
  void initState() {
    super.initState();
    this.getJSONData();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: data != null ? data.length : 0,
      itemBuilder: (context, index) {
        return _buildItemsForListView(data[index]);
      },
    );
  }

  Widget _buildItemsForListView(dynamic item) {
    if (item == null) {
      return CircularProgressIndicator();
    } else {
      return CardTile(
        image: item['urls']['small'] ?? 'No Image!',
        imageDescription:
            item['description'] ?? item['alt_description'] ?? 'No description!',
        author: item['user']['name'] ?? 'Unnamed author!',
        onTapSecondScreen: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SecondScreen(),
              settings:
                  RouteSettings(arguments: item['urls']['full'] ?? 'No Image'),
            ),
          );
        },
      );
    }
  }
}
