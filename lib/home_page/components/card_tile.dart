import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CardTile extends StatelessWidget {
  CardTile({
    this.author,
    this.imageDescription,
    this.image,
    this.onTapSecondScreen,
  });

  final Function onTapSecondScreen;
  final String author;
  final String imageDescription;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.black45,
        borderRadius: BorderRadius.circular(10),
      ),
      height: 200,
      constraints: BoxConstraints(
        maxWidth: 600,
        minWidth: 200,
      ),
      child: Row(
        children: [
          Expanded(
            child: GestureDetector(
              onTap: onTapSecondScreen,
              child: Container(
                height: 200,
                width: 100,
                decoration: BoxDecoration(
                  color: Colors.deepOrange.withOpacity(0.4),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    bottomLeft: Radius.circular(10),
                  ),
                ),
                child: CachedNetworkImage(
                  imageUrl: image,
                  fit: BoxFit.cover,
                  progressIndicatorBuilder: (context, url, downloadProgress) {
                    return Center(
                      child: CircularProgressIndicator(
                          value: downloadProgress.progress),
                    );
                  },
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    author,
                    //textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.deepOrange,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    imageDescription,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
