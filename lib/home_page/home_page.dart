import 'package:flutter/material.dart';

import '../get_started_page/get_started_page.dart';
import 'components/list_tile_builder.dart';

class HomePage extends StatelessWidget {
  static final String id = 'Home Page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Test task'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.clear),
            onPressed: () {
              Navigator.pushNamedAndRemoveUntil(
                context,
                GetStartedPage.id,
                (Route<dynamic> route) => false,
              );
            },
          ),
        ],
      ),
      body: SafeArea(child: ListTileBuilder()),
    );
  }
}
