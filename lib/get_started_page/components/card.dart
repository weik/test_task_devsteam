import 'package:flutter/material.dart';

class MyGetStartedCard extends StatelessWidget {
  final TextStyle textStyle;
  final IconData icon;
  final String text;

  MyGetStartedCard({
    this.textStyle,
    this.icon,
    this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(
        vertical: 10.0,
        horizontal: 25.0,
      ),
      child: ListTile(
        leading: Icon(
          icon,
          color: Colors.deepOrange.withOpacity(0.7),
        ),
        title: Text(
          text,
          style: textStyle,
        ),
      ),
    );
  }
}
