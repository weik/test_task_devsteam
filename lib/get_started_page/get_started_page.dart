import 'package:flutter/material.dart';

import '../get_started_page/components/card.dart';
import '../home_page/home_page.dart';

class GetStartedPage extends StatelessWidget {
  static const String id = 'Get Started Page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ThemeData.dark().primaryColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(
            minRadius: 30,
            maxRadius: 60,
            backgroundImage: AssetImage('assets/avatar.jpg'),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Yaroslav Siomka',
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.white,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Text(
            'TRAINEE FLUTTER DEVELOPER',
            style: TextStyle(
              fontFamily: 'Source Sans Pro',
              fontSize: 15,
              color: Colors.teal[100],
              letterSpacing: 1.5,
              fontWeight: FontWeight.w400,
            ),
          ),
          SizedBox(
            height: 20.0,
            width: 150.0,
            child: Divider(
              color: Colors.teal[100],
            ),
          ),
          MyGetStartedCard(
            icon: Icons.phone,
            text: '+38(093)-224-05-41',
            textStyle: TextStyle(
              fontSize: 20,
              color: Colors.white70,
              letterSpacing: 1.2,
            ),
          ),
          MyGetStartedCard(
            icon: Icons.email,
            text: 'yaroslav.siomka@gmail.com',
            textStyle: TextStyle(
              fontSize: 16,
              color: Colors.white70,
              letterSpacing: 1,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          MaterialButton(
            minWidth: 200,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(15),
              ),
            ),
            child: Text(
              'Get Started !',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
                letterSpacing: 1.2,
              ),
            ),
            color: Colors.deepOrange.withOpacity(0.6),
            onPressed: () {
              Navigator.pushNamedAndRemoveUntil(
                context,
                HomePage.id,
                (Route<dynamic> route) => false,
              );
            },
          ),
        ],
      ),
    );
  }
}
